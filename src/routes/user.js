/*!
 * nodejs-es6-express-seed
 * Copyright (c) 2018 Ibukun O. Dairo
 * MIT Licensed
 */

"use strict";

/**
 * Module dependencies.
 */

import express from "express";
import auth from "../lib/auth"; // import authentication module

// use express router
const router = express.Router();
 
// set up a router and pass it using module.exports
export default router;